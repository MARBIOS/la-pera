# Frases célebres de *Pablo Sanchez* en *LaPeraSessions*:

### Session 1

- *"El Cluster es nuestro laboratorio"*
- *"El backup es otro ordenador diferente que tiene un porrón de discos duros"*
- *Switch = "Super Cacho router"*
- *"Estas lineas que vas escribiendo, si no hay espacio, se pierden en el éter"*
- *"Cuando uno va alocado por la vida, se carga cosas"*

### Session 2

- *"Me apetece que lo rompáis"*
- *"La tercera cola, parece una peli de 3 la tarde en antena3"*  
- *"Los discutimos en el circulo del cluster"*  
- *"Sois unos expertos, me dais miedo"*  
- *"Aqui no somos malos, creo"*  
- *"La cola es un portero con muy mala leche que no deja que se ejecute ningún trabajo que no cumpla una serie de requisitos"* (a.k.a. Teoría **Quiero entrar en tu garito con zapatillas**)
- *"Vamos a paralelizar a tope!"*
- *"Si no sabéis si vuestro programa trabaja en paralelo,* **NO** *trabaja en paralelo"*

### Session 3

- *"Vuestras mierdas gordas las enviais a `scratch`"*
- *"Variables son guays"*

