# NEWS: Without doubts, the word today (session III) is [*ampersand!*](https://es.wikipedia.org/wiki/%26)

## La Pera sessions
This is a **sandbox** for la Pera sessions. Here we can test things, play with the functionalities 
of GitLab pages, write fake issues, break things, repair things... la re-pair!  

![sandbox](sandbox.jpg)

**SECTIONS:**

- **Session notes (Guillem)**
  - [21 July 2016](Session_21_07_2016.md)
  - [28 July 2016](Session_28_07_2016.md)
- **Celebrities**
  - [Frases célebres del profe](Celebrities.md)
  - [Frases célebres de los alumnillos](Celebrities_others.md)