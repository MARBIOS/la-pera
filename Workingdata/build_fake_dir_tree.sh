#!/bin/bash

# generate directory tree for Joan's problem

# Build directories using text expansion between braces
mkdir -p org1ID/gene{1,2}
mkdir -p org1ID/gene1/{file,gene1}
mkdir -p org2ID/gene1/myscript

# create empty files within the directories
touch org1ID/gene1/file/myfasta.fa org1ID/gene1/gene1/mygene1.txt org1ID/gene2/sequence1.fa org2ID/gene1/myscript/sequence1.fa
